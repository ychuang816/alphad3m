D3M AutoMLs Systems
===================

`d3m-interface` supports four D3M AutoML systems:

- `AlphaD3M <https://gitlab.com/ViDA-NYU/d3m/alphad3m>`__
- `CMU <https://gitlab.com/sray/cmu-ta2>`__
- `SRI <https://github.com/daraghhartnett/sri_tpot>`__
- `TAMU <https://gitlab.com/axolotl1/axolotl>`__
