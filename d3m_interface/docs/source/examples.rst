Examples
==========

You can find different Jupyter notebook examples about how to use `d3m-interface` in our `public repository <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/tree/master/examples>`__:

- `Working with D3M datasets <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/using_d3m_datasets.ipynb>`__
- `Loading CSV datasets <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/using_non_d3m_datasets.ipynb>`__
- `Analyzing text datasets <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/text_problems/explosion_demo.ipynb>`__
- `Running multiple D3M AutoMLs <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/multiple_automls.ipynb>`__
- `Integration with Auctus Datamart <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/datamart_integration.ipynb>`__
- `Export pipelines to Python code <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/export_pipeline_code.ipynb>`__
- `Explaining text models <https://gitlab.com/ViDA-NYU/d3m/d3m_interface/-/blob/master/examples/text_problems/text_model_explanation.ipynb>`__
