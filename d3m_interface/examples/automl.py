from d3m_interface import AutoML

output_path = '/usr/src/app'
train_dataset = '/usr/src/app/datasets/training_datasets/seed_datasets_archive/185_baseball/TRAIN'
test_dataset = '/usr/src/app/datasets/training_datasets/seed_datasets_archive/185_baseball/TEST'
score_dataset = '/usr/src/app/datasets/training_datasets/seed_datasets_archive/185_baseball/SCORE'

nyu_automl = AutoML(output_path, 'AlphaD3M')
nyu_automl.search_pipelines(train_dataset, time_bound=5)